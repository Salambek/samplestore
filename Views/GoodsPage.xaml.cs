﻿using SampleStore.Models;
using SampleStore.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace SampleStore.Views
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class GoodsPage : Page
    {
        public GoodVM GoodVM { get; set; }
        public GoodsPage()
        {
            this.InitializeComponent();
            this.GoodVM = new GoodVM(); 
            this.DataContext = this.GoodVM;
        }

        public void AddToCart_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var good = button.DataContext as Good;

            OrdersVM.Instance.AddToCart(good);
        }
        public void Home_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
        public void Cart_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(CartPage));
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            var currentPage = Window.Current.Content as Frame;

            if (currentPage.CanGoBack)
            {
                currentPage.GoBack();
            }
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }
    }
}
