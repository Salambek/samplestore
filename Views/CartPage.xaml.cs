﻿using SampleStore.Models;
using SampleStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace SampleStore.Views
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class CartPage : Page
    {
        public CartPage()
        {
            this.InitializeComponent();
            DataContext = OrdersVM.Instance;
        }
        public void RemoveFromCart_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var order = button?.DataContext as Order;
            OrdersVM.Instance.RemoveFromCart(order);
        }
        public void Home_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            var currentPage = Window.Current.Content as Frame;

            if (currentPage.CanGoBack)
            {
                currentPage.GoBack();
            }
        }

        private void SortSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem selectedItem = SortSelector.SelectedItem as ComboBoxItem;
            string sortBy = selectedItem?.Content.ToString();

            if (sortBy == "По названию")
            {
                OrdersVM.Instance.Orders = new ObservableCollection<Order>(OrdersVM.Instance.Orders.OrderBy(o => o.Good.Name));
                OrdersVM.Instance.SortingOption = "По названию";
            }
            else if (sortBy == "По цене")
            {
                OrdersVM.Instance.Orders = new ObservableCollection<Order>(OrdersVM.Instance.Orders.OrderBy(o => o.Good.Price));
                OrdersVM.Instance.SortingOption = "По цене";
            }
            else
            {
                OrdersVM.Instance.SortingOption = "Не выбрано";
            }
        }

        private void SortSelector_Loaded(object sender, RoutedEventArgs e)
        {
            string sortingOption = OrdersVM.Instance.SortingOption;

            foreach (ComboBoxItem item in SortSelector.Items)
            {
                if (item.Content.ToString() == sortingOption)
                {
                    SortSelector.SelectedItem = item;
                    break;
                }
            }
        }

    }
}
