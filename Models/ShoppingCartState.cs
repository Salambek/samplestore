﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleStore.Models
{
    internal class ShoppingCartState
    {
        public ObservableCollection<Order> Orders { get; set; }
        public int TotalProductsCount { get; set; }
        public double TotalPrice { get; set; }
        public string SortingOption { get; set; }
    }
}
