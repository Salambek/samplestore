﻿using SampleStore.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Windows.Storage;

namespace SampleStore.ViewModels
{
    public class GoodVM : INotifyPropertyChanged
    {
        private Good selectedGood;

        public ObservableCollection<Good> Goods { get; set; }
        public Good SelectedGood
        {
            get { return selectedGood; }
            set
            {
                selectedGood = value;
                OnPropertyChanged("SelectedGood");
            }
        }        
        
        public GoodVM()
        {
            try
            {
                var jsonString = Task.Run(async () =>
                {
                    var localFolder = ApplicationData.Current.LocalFolder;
                    var file = await localFolder.GetFileAsync("Goods.json");
                    return await FileIO.ReadTextAsync(file);
                }).GetAwaiter().GetResult();

                Goods = JsonSerializer.Deserialize<ObservableCollection<Good>>(jsonString);
                //using (var context = new AppDbContext())
                //{
                //    Goods = new ObservableCollection<Good>(context.Goods.ToList());
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Файл Goods.json не найден" );
            }


        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
