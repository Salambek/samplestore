﻿using Microsoft.EntityFrameworkCore;
using SampleStore.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Windows.Storage;

namespace SampleStore.ViewModels
{
    public class OrdersVM : INotifyPropertyChanged
    {
        private Order selectedOrder;
        private ObservableCollection<Order> orders;
        private int totalProductsCount;
        private double totalPrice;
        private string sortingOption;


        private static OrdersVM instance;
        public static OrdersVM Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OrdersVM();
                }
                return instance;
            }
        }

        public ObservableCollection<Order> Orders
        {
            get { return orders; }
            set
            {
                orders = value;
                OnPropertyChanged();
            }
        }

        public int TotalProductsCount
        {
            get { return totalProductsCount; }
            set
            {
                totalProductsCount = value;
                OnPropertyChanged();
            }
        }

        public double TotalPrice
        {
            get { return totalPrice; }
            set
            {
                totalPrice = value;
                OnPropertyChanged();
            }
        }

        public Order SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                selectedOrder = value;
                OnPropertyChanged();
            }
        }
        public string SortingOption
        {
            get { return sortingOption; }
            set 
            { 
                sortingOption = value; 
                OnPropertyChanged();
            }
        }
        public OrdersVM()
        {
            if(Orders != null)
                UpdateTotal();
        }

        public async Task LoadOrders()
        {
            try
            {
                var shoppingCartState = await LoadShoppingCartState();
                Orders = shoppingCartState.Orders;
                TotalProductsCount = shoppingCartState.TotalProductsCount;
                TotalPrice = shoppingCartState.TotalPrice;
                SortingOption = shoppingCartState.SortingOption;
            }
            catch (FileNotFoundException)
            {
                Orders = new ObservableCollection<Order>();
            }
        }

        public async Task SaveOrders()
        {
            var shoppingCartState = new ShoppingCartState
            {
                Orders = Orders,
                TotalProductsCount = TotalProductsCount,
                TotalPrice = TotalPrice,
                SortingOption = SortingOption
            };
            await SaveShoppingCartState(shoppingCartState);
        }

        private async Task<ShoppingCartState> LoadShoppingCartState()
        {
            var localFolder = ApplicationData.Current.LocalFolder;
            var file = await localFolder.GetFileAsync("ShoppingCartState.json");
            var jsonString = await FileIO.ReadTextAsync(file);
            return JsonSerializer.Deserialize<ShoppingCartState>(jsonString);
        }

        private async Task SaveShoppingCartState(ShoppingCartState shoppingCartState)
        {
            var jsonString = JsonSerializer.Serialize(shoppingCartState);
            var localFolder = ApplicationData.Current.LocalFolder;
            var file = await localFolder.CreateFileAsync("ShoppingCartState.json", CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(file, jsonString);
        }
        public void AddToCart(Good good)
        {
            Order existingOrder = null;
            if (Orders != null)
                existingOrder = Orders.FirstOrDefault(o => o.Good.Id == good.Id);

            if (existingOrder != null)
            {
                existingOrder.Count++;
            }
            else
            {
                Orders.Add(new Order { Good = good, Count = 1 });
            }

            UpdateTotal();
        }
        private void UpdateTotal()
        {
            TotalProductsCount = Orders.Count;
            TotalPrice = Orders.Sum(o => o.Good.Price * o.Count);
        }

        public void RemoveFromCart(Order order)
        {
            if (order != null)
            {
                int index = Orders.IndexOf(order);

                if (index != -1)
                {
                    if (order.Count > 1)
                    {
                        order.Count--;
                    }
                    else
                    {
                        Orders.RemoveAt(index);
                    }

                    UpdateTotal();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}