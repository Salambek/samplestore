﻿using SampleStore.Models;
using SampleStore.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.Json;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using System.Threading.Tasks;
using SampleStore.Views;

namespace SampleStore
{
    /// <summary>
    /// Обеспечивает зависящее от конкретного приложения поведение, дополняющее класс Application по умолчанию.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Инициализирует одноэлементный объект приложения. Это первая выполняемая строка разрабатываемого
        /// кода, поэтому она является логическим эквивалентом main() или WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        /// <summary>
        /// Вызывается при обычном запуске приложения пользователем. Будут использоваться другие точки входа,
        /// например, если приложение запускается для открытия конкретного файла.
        /// </summary>
        /// <param name="e">Сведения о запросе и обработке запуска.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Не повторяйте инициализацию приложения, если в окне уже имеется содержимое,
            // только обеспечьте активность окна
            if (rootFrame == null)
            {
                // Создание фрейма, который станет контекстом навигации, и переход к первой странице
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Загрузить состояние из ранее приостановленного приложения
                }

                Window.Current.Content = rootFrame;
            }
            if (rootFrame.Content == null)
            {
                rootFrame.Navigate(typeof(GoodsPage));
            }
            Window.Current.Activate();
            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
                }
                Window.Current.Activate();
            }

            Task.Run(async () =>
            {
                var goods = new List<Good>()
                {
                    new Good { Id = 1, Name = "Reebok 1", Price = 560, ImageUrl = "/Assets/Images/reebok_1.png" },
                    new Good { Id = 2, Name = "Adidas 1", Price = 660, ImageUrl = "/Assets/Images/adidas_1.png" },
                    new Good { Id = 3, Name = "Nike 1", Price = 1200, ImageUrl = "/Assets/Images/nike_1.png" },
                    new Good { Id = 3, Name = "Nike 2", Price = 720, ImageUrl = "/Assets/Images/nike_2.png" },
                    new Good { Id = 3, Name = "Nike 3", Price = 650, ImageUrl = "/Assets/Images/nike_3.png" },
                    new Good { Id = 3, Name = "Nike 4", Price = 660, ImageUrl = "/Assets/Images/nike_4.png" },
                    new Good { Id = 3, Name = "Nike 5", Price = 500, ImageUrl = "/Assets/Images/nike_5.png" },
                    new Good { Id = 3, Name = "Nike 6", Price = 890, ImageUrl = "/Assets/Images/nike_6.png" },
                    new Good { Id = 3, Name = "Nike 7", Price = 900, ImageUrl = "/Assets/Images/nike_7.png" },
                    new Good { Id = 3, Name = "Nike 8", Price = 960, ImageUrl = "/Assets/Images/nike_8.png" },
                    new Good { Id = 3, Name = "Nike 9", Price = 860, ImageUrl = "/Assets/Images/nike_9.png" },
                    new Good { Id = 4, Name = "Puma 1", Price = 560, ImageUrl = "/Assets/Images/puma_1.png" },
                    new Good { Id = 5, Name = "Puma 2", Price = 500, ImageUrl = "/Assets/Images/puma_2.png" },
                    new Good { Id = 7, Name = "Adidas 2", Price = 700, ImageUrl = "/Assets/Images/adidas_2.png" },
                    new Good { Id = 8, Name = "Adidas 3", Price = 1060, ImageUrl = "/Assets/Images/adidas_3.png" },
                    new Good { Id = 9, Name = "Puma 3", Price = 550, ImageUrl = "/Assets/Images/puma_3.png" },
                    new Good { Id = 10, Name = "Puma 4", Price = 960, ImageUrl = "/Assets/Images/puma_4.png" },
                    new Good { Id = 11, Name = "Reebok 2", Price = 860, ImageUrl = "/Assets/Images/reebok_2.png" },
                    new Good { Id = 12, Name = "Reebok 3", Price = 960, ImageUrl = "/Assets/Images/reebok_3.png" },
                    new Good { Id = 14, Name = "Adidas 4", Price = 1120, ImageUrl = "/Assets/Images/adidas_4.png" },
                    new Good { Id = 15, Name = "Adidas 5", Price = 1200, ImageUrl = "/Assets/Images/adidas_5.png" },
                    new Good { Id = 17, Name = "Puma 5", Price = 777, ImageUrl = "/Assets/Images/puma_5.png" },
                    new Good { Id = 18, Name = "Puma 6", Price = 700, ImageUrl = "/Assets/Images/puma_6.png" },
                    new Good { Id = 18, Name = "Puma 8", Price = 600, ImageUrl = "/Assets/Images/puma_8.png" },
                    new Good { Id = 18, Name = "Puma 9", Price = 540, ImageUrl = "/Assets/Images/puma_9.png" },
                    new Good { Id = 19, Name = "Reebok 4", Price = 870, ImageUrl = "/Assets/Images/reebok_4.png" },
                    new Good { Id = 20, Name = "Reebok 5", Price = 770, ImageUrl = "/Assets/Images/reebok_5.png" },
                    new Good { Id = 22, Name = "Reebok 6", Price = 770, ImageUrl = "/Assets/Images/reebok_6.png" },
                };


                var jsonString = JsonSerializer.Serialize(goods);

                StorageFolder localFolder = ApplicationData.Current.LocalFolder;
                StorageFile file = await localFolder.CreateFileAsync("Goods.json", CreationCollisionOption.ReplaceExisting);
                await FileIO.WriteTextAsync(file, jsonString);

            }).GetAwaiter().GetResult();


            OrdersVM.Instance.LoadOrders();

            //using (var context = new AppDbContext())
            //{
            //  context.Database.EnsureCreated();

            //  if (!context.Goods.Any())
            //  {
            //    context.Goods.AddRange(new Good[]
            //    {
            //        new Good { Name = "Reebok", Price = 5600, ImageUrl = "/Assets/Images/reebok_1.png" },
            //        new Good { Name = "Adidas", Price = 5600, ImageUrl = "/Assets/Images/reebok_1.png" },
            //        new Good { Name = "Adidas", Price = 5600, ImageUrl = "/Assets/Images/reebok_1.png" },
            //        new Good { Name = "Adidas", Price = 5600, ImageUrl = "/Assets/Images/reebok_1.png" },
            //        new Good { Name = "Adidas", Price = 5600, ImageUrl = "/Assets/Images/reebok_1.png" }
            //    });
            //    context.SaveChanges();
            //  }
            //  if (!context.Orders.Any())
            //  {
            //    context.Orders.AddRange(new Order[]
            //    {
            //        new Order { Good = context.Goods.FirstOrDefault()}
            //    });
            //    context.SaveChanges();
            //  }
            //}
        }

        /// <summary>
        /// Вызывается в случае сбоя навигации на определенную страницу
        /// </summary>
        /// <param name="sender">Фрейм, для которого произошел сбой навигации</param>
        /// <param name="e">Сведения о сбое навигации</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Вызывается при приостановке выполнения приложения.  Состояние приложения сохраняется
        /// без учета информации о том, будет ли оно завершено или возобновлено с неизменным
        /// содержимым памяти.
        /// </summary>
        /// <param name="sender">Источник запроса приостановки.</param>
        /// <param name="e">Сведения о запросе приостановки.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await OrdersVM.Instance.SaveOrders();
            //TODO: Сохранить состояние приложения и остановить все фоновые операции
            deferral.Complete();
        }
    }
}
